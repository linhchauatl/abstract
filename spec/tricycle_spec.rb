require 'rspec'

require_relative '../tricycle'

class TestTricycle < Tricycle
end

describe Tricycle do
  before :all do
    @test_tricycle = TestTricycle.new
  end

  context 'abstract method' do
    it 'throws exception when call change_gear' do
      expect { @test_tricycle.change_gear(1) }.to raise_exception(RuntimeError,
        "TestTricycle needs to implement 'change_gear' for interface Tricycle!")
    end

    it 'throws exception when call speed_up' do
      expect { @test_tricycle.speed_up(1) }.to raise_exception(RuntimeError,
        "TestTricycle needs to implement 'speed_up' for interface Tricycle!")
    end
  end
end