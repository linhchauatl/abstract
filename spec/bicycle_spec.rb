require 'rspec'

require_relative '../abstract_interface'
require_relative '../bicycle'

class TestBicycle < Bicycle
end

describe Bicycle do
  before :all do
    @test_bicycle = TestBicycle.new
  end

  context 'abstract method' do
    it 'throws exception when call change_gear' do
      expect { @test_bicycle.change_gear(1) }.to raise_exception(AbstractInterface::InterfaceNotImplementedError,
        "TestBicycle needs to implement 'change_gear' for interface Bicycle!")
    end

    it 'throws exception when call speed_up' do
      expect { @test_bicycle.speed_up(1) }.to raise_exception(AbstractInterface::InterfaceNotImplementedError,
        "TestBicycle needs to implement 'speed_up' for interface Bicycle!")
    end
  end
end