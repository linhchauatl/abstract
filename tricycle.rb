class Tricycle

  # Some documentation on the change_gear method
  def change_gear(new_value)
    raise "#{self.class.name} needs to implement 'change_gear' for interface Tricycle!"
  end

  # Some documentation on the speed_up method
  def speed_up(increment)
    raise "#{self.class.name} needs to implement 'speed_up' for interface Tricycle!"
  end

  # Some documentation on the apply_brakes method
  def apply_brakes(decrement)
    # do some work here
  end

end